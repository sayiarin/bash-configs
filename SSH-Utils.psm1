# this will setup a remote machine by adding the public key to the authorized_keys file on remote,
# creating an entry at the ssh config file for ease of access and copying my config files to the remote
# no error checking so use at your own risk
function Set-UpRemoteMachine {
    [CmdletBinding()]
    param(
        [string]
        [Parameter(Mandatory = $true, HelpMessage = 'name of the private key file in your ~/.ssh directory')]
        $IdentityFileName,
        [string]
        [Parameter(Mandatory = $true, HelpMessage = 'the host name or IP address')]
        $HostName,
        [string]
        [Parameter(Mandatory = $false, HelpMessage = '(optional) short name for simpler ssh command')]
        $ShortName,
        [string]
        [Parameter(Mandatory = $false, HelpMessage = '(optional) user name')]
        $UserName = "not-my-real-username-lol"
    )
    BEGIN {
        # the base url for my config files on gitlab
        $BaseConfigURL = "https://gitlab.com/sayiarin/bash-configs/-/raw/main"

        # make sure $ShortName is set
        if ([string]::IsNullOrWhiteSpace($ShortName)) {
            $ShortName = $HostName
        }
    }

    PROCESS {
        # append the public key to the authorized keys on the remote machine to allow login without password
        Get-Content -Path $env:USERPROFILE\.ssh\$IdentityFileName.pub | ssh $UserName@$HostName "umask 077; test -d ~/.ssh || mkdir ~/.ssh ; cat >> ~/.ssh/authorized_keys"

        # add entry to local ssh config for easier connection
        Add-Content -Path $env:USERPROFILE\.ssh\config -Value "
Host $ShortName
    HostName $HostName
    User $UserName
    IdentityFile $env:USERPROFILE\.ssh\$IdentityFileName"

        # copy my bash and vim configs
        ssh $ShortName "curl $BaseConfigURL/.bash_profile -o ~/.bash_profile ; curl $BaseConfigURL/.vimrc -o ~/.vimrc"
    }   
}

Export-ModuleMember -Function Set-UpRemoteMachine
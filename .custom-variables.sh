export EDITOR=vi
export VISUAL=vi
# make sure zsh is the default shell for us, because for some reason
# in my system even after setting it properly with chsh this variable 
# is still set to /bin/bash
export SHELL=$(which zsh)

export PATH=$PATH:/home/rin/programs/intellij/bin

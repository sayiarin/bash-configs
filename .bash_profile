# prompt with time, username, machine and full path in color
PS1="\[\033[35m\]\t\[\033[m\]-\[\033[36m\]\u\[\033[m\]@\[\033[32m\]\h:\[\033[33;1m\]\w\[\033[m\]\$ "

export LC_ALL="en_US.UTF-8"

# dir colors, used by ls, tree and other tools
LS_COLORS='di=1:fi=0:ln=31:pi=5:so=5:bd=5:cd=5:or=31:mi=0:ex=35:*.rpm=90'
export LS_COLORS

# some aliases to pimp build in commands
alias ls='ls -G'
alias ll='ls -lAhtrG'
alias la='ls -lA'

alias please='sudo $(fc -ln -1)'

# and some custom functions
function mcdir() {
        mkdir -p $1 && cd $1
}

function cl(){
        cd $1 && ll
}

function mvcd() {
        mv $@ && for last; do true; done && cd $last
}

# docker utility
# connecting with the shell to a running container
function whalecomm() {
        sudo docker exec -i -t $1 /bin/bash
}

# starting a container with an interactive shell
function whalerun() {
    docker run ${@:2} -it $1 /bin/bash
}
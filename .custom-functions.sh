#####
# add custom functions here using the edit-custom-functions command
#####

# some navigation utilities
function mcdir() {
        mkdir -p $1 && cd $1
}

function cl(){
        cd $1 && ll
}

function mvcd() {
        mv $@ && for last; do true; done && cd $last
}

# docker utility
# connecting with the shell to a running container
function whalecomm() {
        sudo docker exec -i -t $1 /bin/bash
}

# starting a container with an interactive shell
function whalerun() {
    docker run ${@:2} -it $1 /bin/bash
}


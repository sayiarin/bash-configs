function Add-CustomCommandToProfile {
    [CmdletBinding()]
    param(
        [string]
        [Parameter(Mandatory = $true, HelpMessage = 'path to file for module or function')]
        $ModulePath
    )
    BEGIN {
        $ProfilePath = [Environment]::GetFolderPath("MyDocuments") + "\Powershell\Profile.ps1"
    }

    PROCESS {
        if (-Not [System.IO.File]::Exists($ModulePath)) {
            Write-Output ("no such file exists: " + $ModulePath)
            return
        }
        $AbsoluteModulePath = (Resolve-Path -Path $ModulePath).Path

        # append module
        Add-Content -Path $ProfilePath -Value ("`nImport-Module -Name " + $AbsoluteModulePath)
        Write-Output aaaa
    }
}

Export-ModuleMember -Function Add-CustomCommandToProfile
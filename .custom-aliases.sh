#####
# add custom aliases here using the edit-custom-aliases command
#####

# simple config edit commands that automatically source the file once it's saved
alias edit-aliases='${EDITOR:-vi} ~/.custom-aliases.sh && source ~/.custom-aliases.sh'
alias edit-functions='${EDITOR:-vi} ~/.custom-functions.sh && source ~/.custom-functions.sh'
alias edit-variables='${EDITOR:-vi} ~/.custom-variables.sh && source ~/.custom-variables.sh'

# common aliases
alias ls='ls --color'
alias ll='ls -halF'
alias cl='clear'

# nicer than shouting for sudo 
alias please='sudo $(fc -ln -1)'


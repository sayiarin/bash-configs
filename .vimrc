" make sure we have utf-8 enabled
scriptencoding utf-8
set encoding=utf-8

" Enable syntax highlighting
syntax on

" Enable line numbers
set number

" Indentation 
set tabstop=4
set shiftwidth=4
set softtabstop=4
" with spaces instead of tab
set expandtab

" Show “invisible” characters
set lcs=tab:▸\ ,trail:·,eol:¬,nbsp:_
set list

" Highlight cursorline!
set cursorline

" Always show current position
set ruler